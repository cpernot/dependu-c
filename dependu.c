#include <glib.h>
#include <gmodule.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct {
	GArray *possibleWlist;
	int cert;
	GArray *possibleLetters;
} Result;

Result *result_new(){
	Result *r = malloc(sizeof(Result));
	return r;
}

Result *result_init(GArray *possibleWlist, int cert, GArray *possibleLetters){
	Result *r = result_new();
	r->possibleWlist = possibleWlist;
	r->cert = cert;
	r->possibleLetters = possibleLetters;
	return r;
}

GString *result_tostr(Result *r){
	GString *out = g_string_new(NULL);
	g_string_printf(out,"Lettres possible à %d%% :\n", r->cert);
	for(int i = r->possibleLetters->len;i--;){
		g_string_append_unichar(out,g_array_index(r->possibleLetters,gunichar,i));
		if (i)
			g_string_append_unichar(out,',');
	}
	int l = r->possibleWlist->len;
	if (l < 80){
		g_string_append_printf(out,"\n\n%d Possibilités de mots:",l);
		for (int i = 0;i<l;i++){
			gchar *word = g_array_index(r->possibleWlist,gchar *,i);
			g_string_append(out,"\n\t");
			g_string_append(out,word);
		}
	}
	return out;
}

bool isValid(gchar *word, gchar *guess, gchar *absent){
	int lg = g_utf8_strlen(guess,-1);
	int lw = g_utf8_strlen(word,-1);
	gchar *g_curr = guess;
	gchar *w_curr = word;
	if (lg != lw){
		return false;
	}
	bool res = true;
	GString *told = g_string_new(guess);
	g_string_append(told,absent);
	for (int i=0;i<lg;i++){
		gunichar g = g_utf8_get_char(g_curr);
		gunichar w = g_utf8_get_char(w_curr);
		if (!(g == w || (g == '_' && !strchr(told->str,w)))){
			res = false;
			break;
		}
		g_curr = g_utf8_next_char(g_curr);
		w_curr = g_utf8_next_char(w_curr);
	}
	g_string_free(told,true);
	return res;
}

void arr_free(gpointer ptr){
	GArray *arr = (GArray *)ptr;
	for (int i = 0;i<arr->len;i++)
		free(g_array_index(arr,char *,i));
	g_array_free(arr,true);
}

void addEntry(GHashTable *out, gchar *word){
	//out: GHashTable int => GArray[gchar *]
	int len = g_utf8_strlen(word,-1);
	GArray *wordlist = g_hash_table_lookup(out,GINT_TO_POINTER(len));
	if (wordlist == NULL){
		wordlist = g_array_new(false,false,sizeof(gchar *));
		g_hash_table_insert(out,GINT_TO_POINTER(len),wordlist);
	}
	gchar *value = strdup(word);
	g_array_append_val(wordlist,value);
}

GHashTable *Load(gchar *path){
	//return: GHashTable int => GArray[gchar *]
	FILE *fd = fopen(path,"r");
	if (fd == NULL){
		printf("Erreur d'ouverture :%s\n");
		exit(EXIT_FAILURE);
	}
	GHashTable *out = g_hash_table_new_full(g_direct_hash,g_direct_equal,NULL,arr_free);
	char *line = NULL;
	gsize len = 0;
	gssize read;
	while ((read = getline(&line, &len, fd)) != -1){
		line[strlen(line)-1] = '\0';
		addEntry(out,line);
	}
	free(line);
	fclose(fd);
	return out;
}

GArray *getPossibleWList(GHashTable *dict, gchar *guess, gchar *absent){ 
	//return: GArray[gchar *]
	//dict: GHashTable int => GArray[gchar *]
	GArray *out = g_array_new(false,false,sizeof(gchar *));
	int wlen = g_utf8_strlen(guess,-1);
	GArray *toGuess = g_hash_table_lookup(dict,GINT_TO_POINTER(wlen));
	if (toGuess == NULL){
		printf("em: %d\n",wlen);
		return out;
	}
	for (int i = 0; i < toGuess->len;i++){
		gchar *word = g_array_index(toGuess,gchar *,i);
		if (isValid(word,guess,absent)){
			gchar *word_dup = strdup(word);
			g_array_append_val(out,word_dup);
		}
	}
	return out;
}

GArray *differentchars(GArray *wlist){
	//return: GArray[gunichar]
	//wlist: GArray[gchar *]
	GArray *out = g_array_new(false,false,sizeof(gunichar));
	for(int i=0;i<wlist->len;i++){
		for(gchar *word = g_array_index(wlist,gchar *,i);*word;word = g_utf8_next_char(word)){
			gunichar c = g_utf8_get_char(word);
			for(int j=0;j<out->len;j++){
				if(g_array_index(out,gunichar,j) == c){
					goto dont_add; //double break
				}
			}
			g_array_append_val(out,c);
dont_add:;
		}
	}
	return out;
}

Result *getResult(GHashTable *dict,gchar *guess, gchar *absent){
	//dict: GHashTable int => GArray[gchar *]
	GArray *possiblewl = getPossibleWList(dict,guess,absent);// GArray[gchar *]
	GArray *possibleletters = differentchars(possiblewl); // GArray[gunichar]
	GHashTable *lettersoccurences = g_hash_table_new_full(g_direct_hash,g_direct_equal,NULL,free); //GHashTable: gunichar => int*
	for(int i=0;i<possibleletters->len;){
		gunichar letter = g_array_index(possibleletters,gunichar,i);
		if (strchr(guess,letter) || strchr(absent,letter))
			g_array_remove_index(possibleletters,i);
		else
			i++;
	}
	int *occ_ptr;
	for(int i = 0;i<possiblewl->len;i++){
		gchar *word = g_array_index(possiblewl,gchar *,i);
		for (int j = 0;j<possibleletters->len;j++){
			gunichar c = g_array_index(possibleletters,gunichar,j);
			if (strchr(word,c)){
				occ_ptr = g_hash_table_lookup(lettersoccurences,GINT_TO_POINTER(c));
				if (occ_ptr == NULL){
					occ_ptr = (int *)malloc(sizeof(int));
					*occ_ptr = 0;
					g_hash_table_insert(lettersoccurences,GINT_TO_POINTER(c),occ_ptr);
				}
				(*occ_ptr)++;
			}
		}
	}
	GArray *oftenletters = g_array_new(false,false,sizeof(gunichar)); //GArray[gunichar]
	int highval = 0;
	int cert;
	GList *c = g_hash_table_get_keys(lettersoccurences);
	GList *o = g_hash_table_get_values(lettersoccurences);
	GList *c_first = c;
	GList *o_first = o;
	while(c && o && c->data){
		int occ = * (int *)(o->data);
		gunichar letter = (guint64)(c->data);
		if (occ>highval){
			g_array_free(oftenletters,true);
			oftenletters = g_array_new(false,false,sizeof(gunichar));
			highval = occ;
		}
		c = c->next;
		o = o->next;
		if (occ < highval)
			continue;
		g_array_append_val(oftenletters,letter);
	}
	int possibleLength = possiblewl->len;
	if (possibleLength == 0){
		cert = 0;
	} else{
		cert = (highval*100) / possibleLength;
	}
	g_hash_table_destroy(lettersoccurences);
	g_list_free(c_first);
	g_list_free(o_first);
	g_array_free(possibleletters,true);
	return result_init(possiblewl,cert, oftenletters);
}

void result_free(Result *r){
	arr_free(r->possibleWlist);
	g_array_free(r->possibleLetters,true);
	free(r);
}

int main(){
	GHashTable *t = Load("/usr/share/dict/french");
	gchar *guess;
	gchar *absent;
	scanf("%ms",&guess);
	scanf("%ms",&absent);
	Result *r = getResult(t,guess,absent);
	GString *r_str = result_tostr(r);
	printf("%s\n",r_str->str);
	g_string_free(r_str,true);
	result_free(r);
	g_hash_table_destroy(t);
	free(guess);
	free(absent);
	return 0;
}
