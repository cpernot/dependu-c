CC = gcc
SOURCES = dependu.c
BIN = dependu
FLAGS = -g -O0 `pkg-config glib-2.0 --cflags --libs`

all:
	gcc $(SOURCES) -o $(BIN) $(FLAGS)

clean:
	rm -f $(BIN)
